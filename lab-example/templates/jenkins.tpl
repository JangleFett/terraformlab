#!/bin/bash -v

# Installs Jenkins on instance
# Adds the admin user with password secret from XML template
# Addes a single job through the API

# Add Java 8
yum -y install java-1.8.0-openjdk-1.8.0.151-1.b12.35.amzn1.x86_64 java-1.8.0-openjdk-headless-1.8.0.151-1.b12.35.amzn1.x86_64 java-1.8.0-openjdk-devel-1.8.0.151-1.b12.35.amzn1.x86_64
yum -y erase java-1.7.0-openjdk

# Check if Jenkins is already running
if rpm -qa | grep jenkins >/dev/null 2>&1
then
	service jenkins stop >/dev/null 2>&1
	>/var/log/jenkins/jenkins.log
fi

yum -y install java wget
if [[ ! -d /etc/yum.repos.d/jenkins.repo ]]
then
	wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
	rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
	yum -y install jenkins
fi

# Unzip the Jar file
aws s3 cp s3://${bucket}/jenkins-init.tgz /tmp/jenkins-init.tgz
cd /
tar xvf /tmp/jenkins-init.tgz

service jenkins start
chkconfig jenkins on

rm /tmp/jenkins-init.tgz
