/*
  S3 Bucket store for core files
*/

resource "aws_s3_bucket" "storage" {
  bucket =  "${lower(var.my_tag)}-tfstore"
  acl = "public-read-write"

  tags {
    Name = "${var.my_tag}_storage"
  }
}

resource "aws_s3_bucket_policy" "storage" {
  bucket = "${aws_s3_bucket.storage.id}"
  policy =<<POLICY
{
  "Version": "2012-10-17",
  "Id": "Policy${var.my_tag}Bucket",
  "Statement": [
    {
      "Sid": "IPAllow",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:*",
      "Resource": "arn:aws:s3:::${lower(var.my_tag)}-tfstore/*"
    }
  ]
}
POLICY
}

/* Upload files */

resource "aws_s3_bucket_object" "jenkins" {
  depends_on = ["aws_s3_bucket.storage"]
  bucket = "${lower(var.my_tag)}-tfstore"
  key    = "jenkins-init.tgz"
  source = "files/jenkins-init.tgz"
  etag   = "${md5(file("files/jenkins-init.tgz"))}"
}

resource "aws_s3_bucket_object" "petclinic" {
  depends_on = ["aws_s3_bucket.storage"]
  bucket = "${lower(var.my_tag)}-tfstore"
  key    = "petclinic"
  source = "files/petclinic"
  etag   = "${md5(file("files/petclinic"))}"
}

resource "aws_s3_bucket_object" "springpet" {
  depends_on = ["aws_s3_bucket.storage"]
  bucket = "${lower(var.my_tag)}-tfstore"
  key    = "spring-petclinic-2.0.0.jar"
  source = "files/spring-petclinic-2.0.0.jar"
  etag   = "${md5(file("files/spring-petclinic-2.0.0.jar"))}"
}
