/*
  Bastion SSH Server
*/
resource "aws_security_group" "bastion" {
    name = "vpc_bastion"
    description = "Allow incoming SSH connections from the office only."

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.office_cidr}"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "${var.my_tag}_BastionSG"
    }
}

data "template_file" "rds_pc" {
  template = "${file("templates/bastion.tpl")}"

  vars {
    dbsrv = "${aws_db_instance.default.address}"
    dbuser = "${var.db_user}"
    dbpw = "${var.db_password}"
    dbname = "${var.db_name}"
  }
}

resource "aws_instance" "bastion" {
    depends_on = ["aws_db_instance.default"]
    ami = "${lookup(var.amis, var.aws_region)}"
    availability_zone = "${var.aws_region}a"
    instance_type = "t2.micro"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.bastion.id}"]
    subnet_id = "${aws_subnet.public.id}"
    associate_public_ip_address = true
    source_dest_check = false

    root_block_device {
        volume_size = 8
    }

    #user_data = "${file("user-data/bastion.sh")}"
    user_data = "${data.template_file.rds_pc.rendered}"

    tags {
        Name = "${var.my_tag}_Bastion"
    }
}

resource "aws_route53_record" "bastion" {
  zone_id = "${aws_route53_zone.primary.id}"
  name = "bastion.${var.dns_domain}"
  type = "A"
  ttl = "30"

  records = ["${aws_instance.bastion.public_ip}"]
}
