/*
    Build RDS MySQL
*/

resource "aws_security_group" "db" {
  name = "vpc_db"
  description = "Allow traffic on VPC subnets to access DB"

  ingress {
    from_port = 0
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}","${var.office_cidr}"]
  }
  egress {
      from_port = -1
      to_port = -1
      protocol = "icmp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${aws_vpc.default.id}"

  tags {
      Name = "${var.my_tag}_DBSG"
  }
}

resource "aws_db_subnet_group" "default" {
  name = "${lower(var.my_tag)}dbsg"
  subnet_ids = ["${aws_subnet.private.id}","${aws_subnet.private2.id}"]
}

resource "aws_db_instance" "default" {
  allocated_storage = 10
  storage_type = "gp2"
  multi_az = false
  engine = "mysql"
  engine_version = "5.7.16"
  instance_class = "db.m3.medium"
  name = "${var.db_name}"
  username = "${var.db_user}"
  password = "${var.db_password}"
  skip_final_snapshot = true
  vpc_security_group_ids = ["${aws_security_group.db.id}"]
  db_subnet_group_name = "${aws_db_subnet_group.default.name}"
  identifier = "${lower(var.my_tag)}db"

  tags {
      Name = "${var.my_tag}_DB"
  }
}
