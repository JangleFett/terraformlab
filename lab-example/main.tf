/*
  Backend storage for shared state

terraform {
  depends_on = ["aws_s3_bucket.storage","aws_s3_bucket_policy.storage"]
  backend "s3" {
    bucket = "steves-tfstore"
    key    = "terraform.tfstate"
    region = "eu-central-1"
  }
}

data "terraform_remote_state" "tfpetclinic" {
  depends_on = ["aws_s3_bucket.storage","aws_s3_bucket_policy.storage"]
  backend = "s3"
  config {
    bucket = "${lower(var.my_tag)}-tfstore"
    key    = "terraform.tfstate"
    region = "${var.aws_region}"
  }
}

*/
