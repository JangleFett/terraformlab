/*
    Load balancer
*/

resource "aws_security_group" "web" {
    name = "vpc_elb"
    description = "Allow incoming from VPC subnets and 80 from the world"

    ingress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["${var.vpc_cidr}"]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["${var.weblocation}"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "${var.my_tag}_webSG"
    }
}

resource "aws_elb" "web" {
  name               = "petcliniclb"
  # availability_zones = ["${var.aws_region}a"]
  security_groups = ["${aws_security_group.web.id}"]
  # subnets = ["${aws_subnet.public.id}","${aws_subnet.private.id}"]
  subnets = ["${aws_subnet.public.id}"]

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8080/"
    interval            = 30
  }

  instances                   = ["${aws_instance.webinstance.*.id}",]
  cross_zone_load_balancing   = false
  idle_timeout                = 240
  connection_draining         = true

  tags {
    Name = "${var.my_tag}_ELB"
  }
}

resource "aws_route53_record" "weblb" {
  zone_id = "${aws_route53_zone.primary.id}"
  name = "petclinic.${var.dns_domain}"
  type = "CNAME"
  ttl = "30"

  records = ["${aws_elb.web.dns_name}"]
}
