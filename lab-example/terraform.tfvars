/*
  Useful if building multiple environments.
  Use a different file for each environment, dev, test, prod.
  When planning or destroying use --var-file dev.tfvars or --var-file prod.tfvars
*/

aws_access_key = ""
aws_secret_key = ""
db_password = ""
