/*
  Jenkins Server
*/
resource "aws_security_group" "jenkins" {
    name = "vpc_jenkins"
    description = "Allow incoming Web connections."

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["${var.weblocation}"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.weblocation}"]
    }

    ingress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["${var.vpc_cidr}"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "${var.my_tag}_JenkinsSG"
    }
}

data "template_file" "jenkins" {
  template = "${file("templates/jenkins.tpl")}"

  vars {
    bucket = "${lower(var.my_tag)}-tfstore"
  }
}

resource "aws_instance" "jenkins" {
    depends_on = ["aws_s3_bucket_object.jenkins"]
    ami = "${lookup(var.amis, var.aws_region)}"
    availability_zone = "${var.aws_region}a"
    instance_type = "t2.micro"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.jenkins.id}"]
    subnet_id = "${aws_subnet.public.id}"
    associate_public_ip_address = true
    source_dest_check = false
    root_block_device {
        volume_size = 20
    }
    # user_data = "${file("user-data/jenkins.sh")}"
    user_data = "${data.template_file.jenkins.rendered}"
    iam_instance_profile = "s3Vault"

    tags {
        Name = "${var.my_tag}_Jenkins"
    }
}

resource "aws_route53_record" "jenkins" {
  zone_id = "${aws_route53_zone.primary.id}"
  name = "jenkins.${var.dns_domain}"
  type = "A"
  ttl = "30"

  records = ["${aws_instance.jenkins.public_ip}"]
}
