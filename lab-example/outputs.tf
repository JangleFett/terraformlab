output "appDNS" {
  value = "${aws_route53_record.weblb.name}"
}

output "dbEndpoint" {
  value = "${aws_db_instance.default.endpoint}"
}

output "jenkinsDNS" {
  value = "${aws_route53_record.jenkins.name}"
}

output "jenkinsIP" {
  value = "${aws_instance.jenkins.public_ip}"
}

output "dnsZoneId" {
  value = "${aws_route53_zone.primary.id}"
}

output "bastionDNS" {
  value = "${aws_route53_record.bastion.name}"
}

output "bastionIP" {
  value = "${aws_instance.bastion.public_ip}"
}
