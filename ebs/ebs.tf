resource "aws_ebs_volume" "xvdb" {
  count                       = "${var.create_ebs}"
  availability_zone           = "${var.az}"
  size                        = "${var.disk_sizeGB}"
  type                        = "${var.ebs_type}"
  tags {
    Name        = "${var.prefix}-EBSExample"
    Environment = "AL Academy"
  }
}
