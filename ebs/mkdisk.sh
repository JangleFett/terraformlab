if [ ! -d /terraform ]
then
  mkdir /terraform
fi
if ! mount /dev/xvdb1 /terraform
then
  echo -e 'n\np\n\n\n\nw\n' | fdisk /dev/xvdb
  mkfs -t ext4 /dev/xvdb1
fi
if ! grep xvdb /etc/fstab
then
  echo '/dev/xvdb1     /terraform    ext4    defaults    0 2' >>/etc/fstab
fi

if ! df -h | grep '/terraform'
then
  mount /terraform
fi
