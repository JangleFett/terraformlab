terraform {
  backend "s3" {
    bucket = "alacademytemplates"
    key    = "steveprod/simpleVM/terraform.tfstate"
    region = "eu-west-2"
  }
}

data "terraform_remote_state" "simplevm" {
  backend = "s3"
  config {
    bucket = "alacademytemplates"
    key    = "steveprod/simpleVM/terraform.tfstate"
    region = "eu-west-2"
  }
}
