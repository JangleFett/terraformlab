resource "aws_instance" "ec2" {
  ami             = "${var.image_id}"
  instance_type   = "t2.micro"
  key_name        = "steveshilling"
  security_groups = [ "al-office" ]
  associate_public_ip_address = true
  tags {
    Name = "stevesTFvmUpdate"
  }
}
