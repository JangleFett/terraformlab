variable "secret_key" {}
variable "access_key" {}

variable "cidr_block" {
  default = "10.1.0.0/16"
}

variable "pub_sub" {
  default = ["10.1.0.0/18","10.1.64.0/18"]
}

variable "priv_sub" {
  default = ["10.1.128.0/18","10.1.192.0/18"]
}

variable "image_id" {
  type = "map"
  default = {
    "eu-west-2" = "ami-0274e11dced17bb5b"
    "eu-west-3" = "ami-051707cdba246187b"
  }
}

variable "region" {
  default = "eu-west-3"
  type    = "string"
}

variable "ourzoneid" {
  default = "Z2U00Q95U7EKEA"
}

variable "namespace" {
  default = "steve"
  type    = "string"
}

variable "stage" {
  default = "dev"
  type    = "string"
}

variable "name" {
  default = "steveapp"
  type    = "string"
}

variable "azs" {
  default = ["eu-west-3a","eu-west-3b"]
  type    = "list"
}
