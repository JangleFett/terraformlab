resource "aws_route53_record" "www" {
  zone_id = "${var.ourzoneid}"
  name    = "steve"
  type    = "A"
  ttl     = "60"
  records = ["${aws_instance.haproxy.*.public_ip}"]
}
