resource "aws_instance" "web" {
  count           = 4
  ami             = "${lookup(var.image_id,var.region)}"
  instance_type   = "t2.micro"
  key_name        = "steveshilling"
  security_groups = [ "${aws_security_group.allow_all.id}" ]
  subnet_id       = "${count.index%2 == 0 ? "${module.public_subnets.az_subnet_ids["eu-west-3a"]}" : "${module.public_subnets.az_subnet_ids["eu-west-3b"]}"}"
  associate_public_ip_address = false
  tags {
    Name = "stevesWeb"
  }
  user_data       = "${file("files/web.sh")}"
}
